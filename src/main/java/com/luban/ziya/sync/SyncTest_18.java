package com.luban.ziya.sync;

import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.TimeUnit;

/**
 * Created By ziya
 * 2020/9/14
 */
public class SyncTest_18 {

    public static Object object = new SyncTest_18();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            test();
        }, "t1");

        Thread t2 = new Thread(() -> {
            test();
        }, "t2");

        Thread t3 = new Thread(() -> {
            test();
        }, "t3");

        t1.start();
//        TimeUnit.SECONDS.sleep(1);
        t2.start();
//        TimeUnit.SECONDS.sleep(1);
        t3.start();

        t1.join();
        t2.join();
        t3.join();
    }

    public static void test() {
        synchronized (object) {
            System.out.println(Thread.currentThread().getName());

            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
